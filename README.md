# Python Serverless on AWS project sample

This project sample shows the usage of _to be continuous_ templates:

* Python
* Spectral
* SonarQube (from [sonarcloud.io](https://sonarcloud.io/))
* AWS (Amazon Web Services)
* Postman
* Hurl

The project deploys a basic serverless API developped in Python (3.11) on AWS Lambda, and implements automated accetance tests with Postman.

## Python template features

This project uses [setuptools](https://setuptools.pypa.io/en/latest/) as dependency management and build system.
It uses the following features from the GitLab CI Python template:

* Uses the `setup.cfg` build specs file with [Setuptools](https://setuptools.pypa.io/en/latest/index.html) as dependency management,
* Declares `PYTHON_EXTRA_DEPS: "dev"` to install `dev` extra dependencies from `setup.cfg`,
* Enables the [pytest](https://docs.pytest.org/) unit test framework by declaring the `$PYTEST_ENABLED` in the `.gitlab-ci.yml` variables,
* Enables [pylint](https://pylint.pycqa.org/) by declaring the `$PYLINT_ENABLED` in the `.gitlab-ci.yml` variables,
* Enables the [Bandit](https://pypi.org/project/bandit/) SAST analysis job by declaring the `$BANDIT_ENABLED` and skips the `B311`
  test by overriding `$BANDIT_ARGS` in the `.gitlab-ci.yml` variables.

The Python template also enforces:

* [test report integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html),
* and [code coverage integration](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-report-badge).

## Spectral template features

This project uses the [Spectral](https://docs.stoplight.io/docs/spectral) template to continuously Lint the [OpenAPI specification](./burger_maker/burger_maker.openapi.yml).

## SonarQube template features

This project uses the following features from the SonarQube template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
    * source and test folders,
    * code coverage report (from `pytest`),
    * unit test reports (from `pytest`).

## AWS template features

This project uses AWS [Serverless Application Model](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/) 
(SAM) to build and deploy this serverless application on AWS.

This project uses the following features from the AWS template:

* Overrides the Docker image used by the template (`AWS_CLI_IMAGE`) with the [official](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-image-repositories.html) `public.ecr.aws/sam/build-python3.11:latest`.
* Enables review, staging and production environments (by declaring the `$AWS_REVIEW_ENABLED`, `$AWS_STAGING_ENABLED` and `$AWS_PROD_ENABLED` in the project variables); the AWS template implements [environments integration](https://gitlab.com/to-be-continuous/samples/maven-on-gcloud/environments) and review environment cleanup support (manually or when the related development branch is deleted).
* Configures AWS authentication using [OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) (therefore includes the OIDC variant).
* Defines some required secret CI/CD project variables:
    * `$AWS_DEFAULT_REGION`: the default AWS region to deploy to
    * `$AWS_SAM_BUCKET`: the [S3 bucket used by SAM to deploy](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html#ref-sam-cli-deploy-options)
* Enables API custom domain by defining the `$AWS_CUSTOM_DOMAIN_CERT_ARN` CI/CD project variable (can be ommited if this project is cloned).

In order to perform AWS deployments, the project implements:

* `aws-deploy.sh` script: generic deployment script using `sam build` and `sam deploy` commands,
* `aws-cleanup.sh` script: generic cleanup script using the `sam delete` command.

Both scripts make use of variables dynamically evaluated and exposed by the AWS template:

* `${environment_name}`: the application target name to use in this environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`); 
  this is used as the SAM/CloudFormation [stack name](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html),
* `${environment_type}`: the environment type (`review`, `integration`, `staging` or `production`); added as a tag.

Lastly, the deployment script implements the [dynamic way](https://docs.gitlab.com/ee/ci/environments/#set-a-dynamic-environment-url) of
defining the environment URLs: retrieves the generated server URL as a [CloudFormation output](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/outputs-section-structure.html), and dumps it into a `environment_url.txt` file, supported by the template.

## Postman

This project also implements Postman acceptance tests, simply storing test collections in the default [postman/](./postman) directory.

The upstream deployed environment base url is simply referenced in the Postman tests by using the [{{base_url}} variable](https://learning.postman.com/docs/sending-requests/variables/) evaluated by the template.

## Hurl

This project also implements Hurl acceptance tests, simply storing test scripts in the default [hurl/](./hurl) directory.

The upstream deployed environment base url is simply referenced in the Hurl tests by using the [{{base_url}} variable](https://hurl.dev/docs/templates.html#injecting-variables) injected by the template.
