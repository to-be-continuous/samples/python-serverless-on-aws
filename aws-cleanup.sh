#!/usr/bin/env bash
set -e

echo "[aws-cleanup] Cleanup $environment_name..."

# disable AWS CLI pager
export AWS_PAGER=""

function assert_defined() {
  if [[ -z "$1" ]]
  then
    echo -e "$2"
    exit 1
  fi
}

assert_defined "$AWS_DEFAULT_REGION" "Missing required default region (\$AWS_DEFAULT_REGION variable)"
assert_defined "$AWS_SAM_BUCKET" "Missing required SAM S3 bucket (\$AWS_SAM_BUCKET variable)"

sam delete --no-prompts --stack-name "$environment_name" --region "$AWS_DEFAULT_REGION" --s3-bucket "$AWS_SAM_BUCKET"
