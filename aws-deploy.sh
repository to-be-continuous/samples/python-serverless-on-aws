#!/usr/bin/env bash
set -e

echo "[aws-deploy] Deploy $environment_name..."

# disable AWS CLI pager
export AWS_PAGER=""

function assert_defined() {
  if [[ -z "$1" ]]
  then
    echo -e "$2"
    exit 1
  fi
}

assert_defined "$AWS_DEFAULT_REGION" "Missing required default region (\$AWS_DEFAULT_REGION variable)"
assert_defined "$AWS_SAM_BUCKET" "Missing required SAM S3 bucket (\$AWS_SAM_BUCKET variable)"

# try to create bucket if it doesn't exist yet
if ! aws s3 ls "$AWS_SAM_BUCKET"
then
  echo "SAM bucket ($AWS_SAM_BUCKET) not found: try to create..."
  aws s3api create-bucket --bucket "$AWS_SAM_BUCKET" --region "$AWS_DEFAULT_REGION" \
      --create-bucket-configuration LocationConstraint="$AWS_DEFAULT_REGION"
fi

sam_template=template.yaml
sam_params="AppName=$environment_name"
if [[ "$AWS_CUSTOM_DOMAIN_CERT_ARN" ]]
then
  base_domain="${AWS_CUSTOM_DOMAIN_BASE:-samples.aws.to-be-continuous.com}"
  echo "Custom domain enabled: $environment_name.$base_domain (base domain can be overriden with \$AWS_CUSTOM_DOMAIN_BASE)"
  sam_template=template-custom-domain.yaml
  sam_params="AppName=$environment_name BaseDomain=$base_domain CustomDomainCertArn=$AWS_CUSTOM_DOMAIN_CERT_ARN"
else
  echo "Custom domain not enabled: use default AWS execute endpoint"
fi

# 1: build
sam build ${TRACE+--debug} --template $sam_template

# 2: deploy (each environment is a separate stack)
# AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY defined as secret CI/CD variable
# AWS_DEFAULT_REGION and AWS_SAM_BUCKET defined as project CI/CD variable
sam deploy ${TRACE+--debug} \
  --template $sam_template \
  --stack-name "$environment_name" \
  --region "$AWS_DEFAULT_REGION" \
  --s3-bucket "$AWS_SAM_BUCKET" \
  --no-fail-on-empty-changeset \
  --no-confirm-changeset \
  --parameter-overrides "$sam_params" \
  --tags "ci-job-url=$CI_JOB_URL environment=$environment_type"

# Retrieve outputs (use cloudformation query)
api_url=$(aws cloudformation describe-stacks --stack-name "$environment_name" --output text --query 'Stacks[0].Outputs[?OutputKey==`ApiUrl`].OutputValue')

echo "Stack created/updated:"
echo " - Api URL: $api_url"

# Finally set the dynamically generated WebServer Url
echo "$api_url" > environment_url.txt
