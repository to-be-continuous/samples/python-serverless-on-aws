import random
import uuid
from dataclasses import dataclass
from typing import Any

from aws_lambda_powertools import Logger, Metrics, Tracer
from aws_lambda_powertools.event_handler import APIGatewayRestResolver, Response
from aws_lambda_powertools.event_handler.exceptions import NotFoundError
from aws_lambda_powertools.logging import correlation_paths
from aws_lambda_powertools.utilities.data_classes import APIGatewayProxyEvent
from aws_lambda_powertools.utilities.typing import LambdaContext

# https://awslabs.github.io/aws-lambda-powertools-python/#features
tracer = Tracer()
logger = Logger()
metrics = Metrics()
app = APIGatewayRestResolver(enable_validation=True)
app.enable_swagger(
    path="/swagger",
    title="Burger Maker API",
    summary="View and order our delicious burgers!",
    description="This API allows to view the available burger recipes and order a burger.<br/>It is a <a target='_blank' href='https://to-be-continuous.gitlab.io/'>to-be-continuous</a> project sample.",
    version="1.0.0",
)


@dataclass
class Ingredient:
    name: str
    id: str


@dataclass
class Burger:
    name: str
    id: str
    ingredients: list[Ingredient]


burger_recipes = {
    "cheeseburger": [
        "bun",
        "red onion",
        "tomato",
        "ketchup",
        "salad",
        "cheddar",
        "steak",
    ],
    "bacon": ["bun", "bacon", "garlic sauce", "brie", "steak", "tomato"],
    "farmer": ["bun", "red onion", "bbq sauce", "stilton", "chicken", "tomato"],
    "fish": ["bun", "tartare sauce", "cucumber", "old gouda", "fish fillet"],
    "veggie": [
        "bun",
        "goat cheese",
        "tomato confit",
        "red onion",
        "super-secret veggie steak",
    ],
    "frenchie": ["baguette", "butter", "brie", "smoked ham"],
}


@app.get(
    "/",
    include_in_schema=False,
)
def redirect_to_swagger() -> None:
    return Response(
        status_code=302,
        headers={"location": "/swagger"},
    )


@app.get(
    "/recipes",
    summary="Get the menu",
    description="Get the list of burger recipe names",
    operation_id="get_menu",
    tags=["recipes"],
    responses={
        200: {"description": "Successfully returned the recipes"},
    },
)
def gime_the_menu() -> list[str]:
    return list(burger_recipes.keys())


@app.get(
    "/recipes/<recipe>",
    summary="Get details about a burger",
    description="Get the list of ingredients of the given burger recipe",
    operation_id="get_recipe",
    tags=["recipes"],
    responses={
        200: {"description": "Successfully returned the list of ingredients"},
        404: {"description": "Recipe not found"},
    },
)
def gime_the_ingredients(recipe: str) -> list[str]:
    ingredients = burger_recipes.get(recipe)
    if ingredients is None:
        raise NotFoundError(f"No such recipe: {recipe}")
    return ingredients


@app.delete(
    "/burgers/<recipe>",
    summary="Order a burger",
    description="Order a burger of the given choice",
    operation_id="order_burger",
    tags=["orders"],
    responses={
        200: {"description": "Successfully prepared the requested burger"},
        404: {"description": "Requested recipe not found"},
    },
)
def prepare_a_burger(recipe: str) -> Burger:
    if recipe == "any":
        # pick one at random
        recipe = random.choice(list(burger_recipes.keys()))

    ingredients = burger_recipes.get(recipe)
    if ingredients is None:
        raise NotFoundError(f"No such recipe: {recipe}")

    return Burger(
        name=recipe,
        id=str(uuid.uuid1()),
        ingredients=list(
            map(lambda name: Ingredient(id=str(uuid.uuid1()), name=name), ingredients)
        ),
    )


@metrics.log_metrics(capture_cold_start_metric=True)
@logger.inject_lambda_context(correlation_id_path=correlation_paths.API_GATEWAY_REST)
@tracer.capture_lambda_handler
def lambda_handler(event: APIGatewayProxyEvent, context: LambdaContext) -> Any:
    """Sample pure Lambda function
    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format
    context: object, required
        Lambda Context runtime methods and attributes
    Returns
    -------
    API Gateway Lambda Proxy Output Format: dict
    """
    return app.resolve(event, context)
